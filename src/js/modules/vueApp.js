Vue.use(VeeValidate);

VeeValidate.Validator.extend('inputCheck', {
  getMessage: () => `Use only letters, numbers and ! ? , . - ‘ “`,
  validate: (value) => {
    const regex = new RegExp('^[A-Za-z0-9!?,."\'\\s]*$');
    return regex.test(value);
  },
});

const vueApp = () => {
  if (Vue) {
    new Vue({
      el: '#app',
      data: {
        userData: [],
        filteredUserData: [],
        userIdFilterArr: [],
        selected: 'all',
        search: '',
        searchParam: '',
        url: 'https://jsonplaceholder.typicode.com/posts',
      },
      template: `
        <div id="app">
          <div class="aside">
            <select v-model="selected">
              <option value="all">All</option>
              <option v-for="item in userIdFilterArr" :value="item">User {{ item }}</option>
            </select>
            <div class="search-wrap">
              <input v-validate="'inputCheck'" type="search" placeholder="Search by title" v-model="search" name="search">
              <span>{{ errors.first('search') }}</span>
              <button @click="handleSearch">Search</button>
            </div>
          </div>
          <ul class="list">
            <li class="item" v-for="item in renderUserData">
              <div class="img-wrap">
                <img v-if="item.imgUrl" :src="item.imgUrl">
                <img v-else src="https://via.placeholder.com/150/ececec">
              </div>
              <span class="title">{{ item.title }}</span>
            </li>
          </ul>
        </div>
      `,
      created() {
        fetch(this.url).then((response) => {
          if (response.ok) {
            response.json().then((json) => {
              this.userData = json;
              this.countValues();
            });
          } else console.log('Error while getting data...');
        });

        // render list according to params in the URL (if they exist)
        const url = new URLSearchParams(window.location.search);
        if (url.get('userId')) this.selected = parseInt(url.get('userId'), 10);
        if (url.get('search')) {
          this.searchParam = url.get('search');
          this.search = url.get('search');
        }
      },
      computed: {
        // render user cards with non-hidden status & sort by 'userId & search value'
        renderUserData() {
          const selectedUserId = this.selected;
          const selectedTitle = this.searchParam;

          return this.userData.filter((elem) => {
            if (selectedUserId === 'all') {
              if (selectedTitle === '') {
                window.history.replaceState({}, null, window.location.pathname);
                return elem.status !== 'hidden';
              }
              this.setParam(null, 'search', this.searchParam, true);
              return elem.status !== 'hidden' && elem.title.toLowerCase().includes(selectedTitle.toLowerCase());
            }
            if (selectedTitle === '') {
              this.setParam(null, 'userId', this.selected, true);
              return elem.userId === selectedUserId && elem.status !== 'hidden';
            }
            this.setParam(null, `?userId=${this.selected}&search=${this.searchParam}`, null, false);
            return elem.userId === selectedUserId && elem.status !== 'hidden'
              && elem.title.toLowerCase().includes(selectedTitle.toLowerCase());
          });
        },
      },
      methods: {
        // add selected 'userId' to the URL as a parameter
        setParam(dataParam, key, val, single) {
          if (single) window.history.pushState(dataParam, null, `?${key}=${val}`);
          else window.history.pushState(dataParam, null, `${key}`);
        },
        // count all values of 'userId' for 'select' filter
        countValues() {
          this.userData.forEach((item) => {
            if (!this.userIdFilterArr.includes(item.userId)) this.userIdFilterArr.push(item.userId);
          });
        },
        // change value of 'searchParam' to trigger filtering
        handleSearch() {
          this.$validator.validate().then((valid) => {
            if (valid) this.searchParam = this.search;
          });
        },
      },
    });
  }
};

const init = () => {
  vueApp();
};

export default init;
